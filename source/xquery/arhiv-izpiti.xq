<page>
  <title>Архив - Изпити</title>
  <content>
    <div class="flex one two-500 three-800 four-1200">{
      for $ekzameno in ekzamenoj/ekzameno
      let $dato := data($ekzameno/dato)
      return
      <div>
        <a href="{$ekzameno/url}">
          <article class="card">
            <header>{$dato}</header>
            <img src="{$ekzameno/bildeto}" alt="Изпит {$dato}"/>
          </article>
        </a>
      </div>
    }</div>
  </content>
</page>

<ekzamenoj>{
  let $ekzamenoj := doc("https://lernu-libera.gitlab.io/matematiko-7-nvo/arkivo/ekzamenoj.xml")
  let $similaj := doc("https://lernu-libera.gitlab.io/matematiko-7-nvo/generita-problemoj/similaj.xml")
  for $ekzameno in $ekzamenoj/ekzamenoj/ekzameno
  let $dato := data($ekzameno/dato)
  return
  <ekzameno>
    {$ekzameno/dato}
    {$ekzameno/url}
    {$ekzameno/bildeto}
    <problemoj>{
      for $problemo in $ekzameno/problemoj/problemo
      let $numero := data($problemo/numero)
      return
      <problemo>
        {$problemo/klaso}
        {$problemo/numero}
        {$problemo/branĉo}
        {$problemo/temo}
        {$problemo/url}
        {$problemo/bildeto}
        {$similaj/ekzamenoj/ekzameno[dato eq $dato]/problemoj/problemo[numero eq $numero]/similaj}
      </problemo>
    }</problemoj>
    {$ekzameno/dato}
  </ekzameno>
}</ekzamenoj>

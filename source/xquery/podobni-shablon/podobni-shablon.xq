<page>
  <title>Подобни на задача {/ekzamenoj/ekzameno/problemoj/problemo/numero} от {/ekzamenoj/ekzameno/dato[1]}</title>
  <content>
    <table>
      <tr>
        <th>№</th>
        <th>
          Задача
        </th>
        <th colspan="2">
          Отговор
        </th>
      </tr>{
      for $problemo at $index in /ekzamenoj/ekzameno/problemoj/problemo/similaj/problemo/latex
      let $kondixco := data($problemo/kondiĉo), $respondo := data($problemo/respondo)
      return
      <tr>
        <td><span class="zadachaNomer">{$index}.</span></td>
        <td>{$kondixco}</td>
        <td class="hoverKey">
          <img src="bootstrap-icons-1.5.0/eyeglasses.svg" alt="Eyeglasses" class="icon"/>
        </td>
        <td class="hoverAnswer">{$respondo}</td>
      </tr>
    }</table>
  </content>
</page>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
  <xsl:template match="/">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
    <html lang="bg">
      <head>        
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title><xsl:value-of select="page/title"/></title>
        <!-- Picnic CSS -->
        <link rel="stylesheet" href="picnic.css"/>
        <!-- Палитра -->
        <link rel="stylesheet" href="palette.css"/>
        <!-- Допълнителен css файл -->
        <link rel="stylesheet" href="my-style.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=TeX-MML-AM_CHTML"></script>
      </head>
      <body>
        <header xmlns="http://www.w3.org/1999/xhtml">
          <nav>
            <a href="index.xhtml" class="brand">
              <img class="logo" src="logo.png" alt="Логото на сайта"/>
              <span>Lernu Libera</span>
            </a>
            
            <!-- responsive-->
            <input id="bmenub" type="checkbox" class="show"/>
            <label for="bmenub" class="burger pseudo button">Меню</label>

            <div class="menu">
              <a href="index.xhtml">
                <xsl:attribute name="class"><xsl:if test="not(page/title = 'Начало')">pseudo</xsl:if> button</xsl:attribute>
                Начало
              </a>
              <a href="arhiv.xhtml">
                <xsl:attribute name="class"><xsl:if test="not(page/title = 'Архив')">pseudo</xsl:if> button</xsl:attribute>
                Архив
              </a>
              <a href="uprazhnenie.xhtml">
                <xsl:attribute name="class"><xsl:if test="not(page/title = 'Упражнениe')">pseudo</xsl:if> button</xsl:attribute>
                Упражнение
              </a>
              <a href="https://lernu-libera.gitlab.io/matematiko-7-nvo/lernolibro/lernolibro.pdf">
                <xsl:attribute name="class"><xsl:if test="not(page/title = 'Сборник')">pseudo</xsl:if> button</xsl:attribute>
                Сборник
              </a>
              <a href="za-saita.xhtml">
                <xsl:attribute name="class"><xsl:if test="not(page/title = 'За сайта')">pseudo</xsl:if> button</xsl:attribute>
                За сайта
              </a>
            </div>
          </nav>  
          <h1><xsl:value-of select="page/title"/></h1>
        </header>
        <main>
          <xsl:copy-of select="page/content/*"/>
        </main>
        <footer>
          Това е свободно място в мрежата, с лиценз <a rel="license" href="http://creativecommons.org/licenses/by-sa/2.5/bg/"><img src="cc-by-sa.svg" class="icon" alt="Икона на лиценза"/></a>. Повече подробности за свободите и отговорностите при ползването му ще откриете в <a href="za-saita.xhtml">"За сайта"</a>
        </footer>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
